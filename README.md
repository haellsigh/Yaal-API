**Yaal-API**
========

Yet Another Anime List open source project api.


----------
## MongoDB Server ##
**START THE SERVER** 
```
> $ parts start mongodb  
```

**STOP THE SERVER**
```
> $ parts stop mongodb
```

**OPEN MONGODB SHELL**
```
> $ mongo
```

## Apache PHP Server ##

**To start the Apache server:**
```
>  $ parts start apache2
```
**To stop the Apache server:**
```
>  $ parts stop apache2
```

**Apache config is located at:**
```
>  $ /home/codio/.parts/etc/apache2/httpd.conf
```

**Default document root is located at:**
```
>  $ /home/codio/workspace
```

**PHP config file is located at:**
```
>  $ /home/codio/.parts/etc/php5/php.ini
```

**If Apache2 httpd is already running, you will need to restart it:**
```
> $ parts restart apache2
```

## Yaal-API Server ##
```
> python run.py
```
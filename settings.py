SERVER_NAME = '127.0.0.1:6112'
RESOURCE_METHODS = ['GET', 'POST', 'DELETE']
ITEM_METHODS = ['GET', 'PATCH', 'PUT', 'DELETE']
X_DOMAINS = '*'
X_HEADERS = ['Content-Type']
DEBUG = True
XML = False

# {
#   'title': 'No Game No Life',
#   'synopsis': 'The story of No Game, No Life centers around Sora and Shiro, a brother and sister whose reputations as brilliant NEET (Not in Education, Employment, or Training) hikikomori (shut-in) gamers have spawned urban legends all over the Internet. These two gamers even consider the real world as just another \'crappy game.\' One day, they are summoned by a boy named \'God\' to an alternate world. There, God has prohibited war and declared this to be a world where \'everything is decided by games\'—even national borders. Humanity has been driven back into one remaining city by the other races. Will Sora and Shiro, the good-for-nothing brother and sister, become the \'Saviors of Humanity\' on this alternate world? \'Well, let's start playing.\'',
#   'episodes': 12,
#   'watched_episodes': 2,
#   'watched_status': 'watching'
# }

animeschema = {
    # Schema definition, based on Cerberus grammar. Check the Cerberus project
    # (https://github.com/nicolaiarocci/cerberus) for details.

    # 'identifier' : String : Unique identifier for the anime.
    # It's usually the Anime name without spaces/special characters.
    # We'll use that value to get the anime.
    'identifier': {
    	'type': 'string',
    	'required': True,
    	'unique': True,
    	'minlength': 1,
    },
    # 'title' : String : Title of the anime.
    'title': {
        'type': 'string',
        'required': True,
    },
    # 'synopsis' : String : Synopsis of the anime.
    'synopsis': {
        'type': 'string',
        'required': True
    },
    # 'genres' : List[String] : Genres of the anime.
    'genres': {
        'type': 'list',
        'allowed': ["Action","Adventure","Cars","Comedy","Dementia","Demons","Drama","Ecchi","Fantasy","Game","Harem","Hentai","Historical","Horror","Josei","Kids","Magic","Martial Arts","Mecha","Military","Music","Mystery","Parody","Police","Psychological","Romance","Samurai","School","Sci-Fi","Seinen","Shoujo","Shoujo Ai","Shounen","Shounen Ai","Slice of Life","Space","Sports","Super Power","Supernatural","Thriller","Vampire","Yaoi","Yuri"]
    },
    # 'status' : String : Status of the anime.
    'status': {
        'type': 'string',
        'allowed': ["finished airing","currently airing","not yet aired"]
    },
    # 'episodes' : integer : Total episodes of the anime.
    'episodes': {
        'type': 'integer',
        'min': 0
    }
}

animes = {
    # by default the standard item entry point is defined as
    # '/anime/<ObjectId>'. We leave it untouched, and we also enable an
    # additional read-only entry point. This way consumers can also perform
    # GET requests at '/animes/<identifier>'.
    'additional_lookup': {
        'url': 'regex("[\w]+")',
        'field': 'identifier'
    },

    # We choose to override global cache-control directives for this resource.
    'cache_control': 'max-age=10,must-revalidate',
    'cache_expires': 10,

    # most global settings can be overridden at resource level
    'resource_methods': ["GET", "POST"],

    'schema': animeschema
}

animelistsschema = {
    # Schema definition, based on Cerberus grammar. Check the Cerberus project
    # (https://github.com/nicolaiarocci/cerberus) for details.

    # 'username' : String : Username of the creator of the list.
    # We'll use that value to get the animelists.
    'username': {
        'type': 'string',
        'required': True,
        'unique': True
    },
    # 'title' : String : Title of the anime.
    'animelists': {
        'type': 'list',
        'schema':   {
            'type': 'dict',
            'schema':   {
                    # 'identifier' : String : Unique identifier for the anime.
                    'identifier':       {
                        'type': 'string',
                        'required': True
                                        },
                    # 'watched_episodes' : integer : Watched episodes.
                    'watched_episodes': {
                        'type': 'integer',
                        'min': 0
                                         },
                    # 'watched_status' : integer : Watching status.
                    'watched_status': {
                        'type': 'string',
                        'allowed': ["watching", "completed", "on-hold", "dropped", "plan to watch"]
                                         },
                    # 'score' : integer : Score of the anime.
                    'score':             {
                        'type': 'integer',
                        'min': 0,
                        'max': 10
                                         }
                        }
                    }
    }
}

animelists = {
    # by default the standard item entry point is defined as
    # '/animelists/<ObjectId>'. We leave it untouched, and we also enable an
    # additional read-only entry point. This way consumers can also perform
    # GET requests at '/animelists/<username>'.
    'additional_lookup': {
        'url': 'regex("[\w]+")',
        'field': 'username'
    },

    # We choose to override global cache-control directives for this resource.
    'cache_control': 'max-age=10,must-revalidate',
    'cache_expires': 10,

    # most global settings can be overridden at resource level
    'resource_methods': ["GET", "POST"],

    'schema': animelistsschema
}

DOMAIN = {
    'animes': animes,
    'animelists': animelists,
}